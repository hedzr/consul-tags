/*
 * Copyright © 2019 Hedzr Yeh.
 */

package consul_tags

const (
	AppName    = "consul-tags" //
	Version    = "0.6.1"       //
	VersionInt = 0x000601      // using as
)
